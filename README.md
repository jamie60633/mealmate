# mealmate



## Getting started

create docker volume for mealmate app

```
docker volume create mealmate-data
```

build and start
```
docker-compose build
docker-compose up
```