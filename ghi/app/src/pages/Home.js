import React, { useEffect, useState } from 'react';

const Home = () => {
  const subscriberUrl = 'http://localhost:8080/api/';
  const chefUrl = 'http://localhost:8081/api/';
  const [subDataOk, setSubDataOk] = useState(false);
  const [chefDataOk, setChefDataOk] = useState(false);
  const testFetch = async () => {
    const subscriberResponse = await fetch(subscriberUrl);
    if (subscriberResponse.ok) {
      const subscriberData = await subscriberResponse.json();
      console.log(subscriberData);
      setSubDataOk(true);
    }
    const chefResponse = await fetch(chefUrl);
    if (subscriberResponse.ok) {
      const chefData = await chefResponse.json();
      console.log(chefData);
      setChefDataOk(true);
    }
  };
  useEffect(() => {
    testFetch();
  }, []);
  return (
    <div>
      <h1>Subscriber Data: {subDataOk ? 'OK' : 'CONNECTING'}</h1>
      <h1>Chef Data: {chefDataOk ? 'OK' : 'CONNECTION'}</h1>
    </div>
  );
};

export default Home;
